<?php
use Phalcon\Loader;
use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\View;
use Phalcon\Mvc\Url as BaseUrl;
use Phalcon \Mvc\Application;

// definisikan BASE_PATH
define("BASE_PATH", dirname(__DIR__));
define("APP_PATH", BASE_PATH . '/app');

// 1. Registrasi komponen autoloaders (Controller, Model
$loader = new Loader();
$loader->registerDirs(
	[
		APP_PATH. '/controller',
		APP_PATH. '/model'
	]
);
$loader->register();

// 2. Konfigurasi service dan meregisterkannya ke dalam depedency injection
$di = new FactoryDefault();

$di->set(
	'view',
	function() {
		$view = new View();
		$view->setViewsDir(APP_PATH. '/view');
		return $view;
	}
);

$di->set(
	'url',
	function() {
		$url = new BaseUrl();
		$url = setBaseUri('/phalcon-basic');
		return $url;
	}
);

$application = new Application($di);
try {
	$response = $application->handle();
	$response->send();
} catch(\Exception $e) {
	echo "Error ", $e->getMessage();
}

// 3. menghandle HTTP request
